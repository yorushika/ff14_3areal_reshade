//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ReShade effect file
// Adaption by brussell
//
// Credits:
// luluco250 - luminance get/store code from Magic Bloom
// Marty McFly - general file structure from MasterEffect
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


/*==============================================================================*\
|                             EFFECT PARAMETERS                                 |
\*==============================================================================*/

#define iAdp_Mode               1       //[1 to 3]     Determines the function to calculate adaption based on average screen luminance. 1=Linear, 2=Quadratic, 3=Inverted Quadratic.
#define fAdp_Speed              0.08    //[0.0 to 1.0] Speed of adaption. The higher the faster.
#define fAdp_ThresholdBrighten  0.2     //[0.0 to 1.0] A lower average screen luminance brightens the image.
#define fAdp_ThresholdDarken    0.3     //[0.0 to 1.0] A higher average screen luminance darkens the image.
#define fAdp_MaxBrighten        0.0    //[0.0 to 1.0] Brightens the image by maximum value.
#define fAdp_MaxDarken          0.3     //[0.0 to 1.0] Darkens the image by maximum value.
#define fAdp_BrightenStatic     0.5     //[0.0 to 1.0] Amount of static brightening.
#define fAdp_BrightenDynamic    0.5     //[0.0 to 1.0] Amount of dynamic (pixel dependent) brightening.
#define fAdp_DarkenStatic       0.5     //[0.0 to 1.0] Amount of static darkening.
#define fAdp_DarkenDynamic      0.5     //[0.0 to 1.0] Amount of dynamic (pixel dependent) darkening.
#define fAdp_BlackProtection    0.5     //[0.0 to 1.0] Amount of lows preservation. 1=no black brightening.
#define fAdp_WhiteProtection    0.5     //[0.0 to 1.0] Amount of highs preservation. 1=no white darkening.
#define iAdp_DebugOutput        0       //[0 or 1]     Displays the average screen luminance.

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// END OF TWEAKING VARIABLES                                                                                                 //
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//global vars
#define LumCoeff    float3(0.212656, 0.715158, 0.072186)
uniform float Frametime < source = "frametime";>;

//textures and samplers
texture2D texColor : COLOR;

sampler2D SamplerColor {Texture = texColor;};

texture2D texLuminance { Width = 256; Height = 256; Format = R8; MipLevels = 7; };
texture2D texAvgLuminance { Format = R16F; };
texture2D texAvgLuminanceLast { Format = R16F; };

sampler SamplerLuminance { Texture = texLuminance; };
sampler SamplerAvgLuminance { Texture = texAvgLuminance; };
sampler SamplerAvgLuminanceLast { Texture = texAvgLuminanceLast; };

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Vertex Shaders                                                                                                            //
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

struct VS_OUTPUT_POST
{
    float4 vpos : SV_Position;
    float2 txcoord : TEXCOORD0;
};

struct VS_INPUT_POST
{
    uint id : SV_VertexID;
};


VS_OUTPUT_POST VS_Main(VS_INPUT_POST IN)
{
    VS_OUTPUT_POST OUT;
    OUT.txcoord.x = (IN.id == 2) ? 2.0 : 0.0;
    OUT.txcoord.y = (IN.id == 1) ? 2.0 : 0.0;
    OUT.vpos = float4(OUT.txcoord * float2(2.0, -2.0) + float2(-1.0, 1.0), 0.0, 1.0);
    return OUT;
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Adaption                                                                                                                  //
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

float PS_Luminance(VS_OUTPUT_POST IN) : COLOR
{
   return dot(tex2Dlod(SamplerColor, float4 (IN.txcoord.xy,0,0)).xyz, LumCoeff);
}

float PS_AvgLuminance(VS_OUTPUT_POST IN) : COLOR
{
   float lum = tex2Dlod(SamplerLuminance, float4(0.5.xx, 0, 7)).x;
   float lumlast = tex2D(SamplerAvgLuminanceLast, 0.0).x;
   return lerp(lumlast, lum, fAdp_Speed * 10/Frametime);
}

float PS_StoreAvgLuminance(VS_OUTPUT_POST IN) : COLOR
{
   return tex2D(SamplerAvgLuminance, 0.0).x;
}

float4 PS_Adaption(VS_OUTPUT_POST IN) : COLOR
{

    float4 color = tex2Dlod(SamplerColor, float4 (IN.txcoord.xy,0,0));

    float avglum = tex2D(SamplerAvgLuminance, 0.0).x;
    float pixellum = dot(color.xyz, LumCoeff);
    [branch]
    if(avglum < fAdp_ThresholdBrighten) {
        #if (iAdp_Mode == 1)
            float brighten_static = (-fAdp_MaxBrighten/fAdp_ThresholdBrighten)*(avglum-fAdp_ThresholdBrighten);
        #elif (iAdp_Mode == 2)
            float brighten_static = (fAdp_MaxBrighten/pow(fAdp_ThresholdBrighten,2.0))*pow((avglum-fAdp_ThresholdBrighten),2.0);
        #elif (iAdp_Mode == 3)
            float brighten_static = (-fAdp_MaxBrighten/pow(fAdp_ThresholdBrighten,2.0))*pow(avglum,2.0)+fAdp_MaxBrighten;
        #endif
        float brighten_dynamic = brighten_static * (1-pixellum);
        float brighten = fAdp_BrightenStatic * brighten_static + fAdp_BrightenDynamic * brighten_dynamic;
        brighten = lerp(brighten, min(pixellum, brighten), fAdp_BlackProtection);
        color.xyz += clamp(brighten, 0, fAdp_MaxBrighten);
    }
    [branch]
    if(avglum > fAdp_ThresholdDarken) {
        #if (iAdp_Mode == 1)
            float darken_static = (-fAdp_MaxDarken/(1-fAdp_ThresholdDarken))*(avglum-fAdp_ThresholdDarken);
        #elif (iAdp_Mode == 2)
            float darken_static = (-fAdp_MaxDarken/pow(1-fAdp_ThresholdDarken,2.0))*pow((avglum-fAdp_ThresholdDarken),2.0);
        #elif (iAdp_Mode == 3)
            float darken_static = (fAdp_MaxDarken/pow((fAdp_ThresholdDarken-1),2.0))*pow((avglum-1),2.0)-fAdp_MaxDarken;
        #endif
        float darken_dynamic = darken_static * pixellum;
        float darken = fAdp_DarkenStatic * darken_static + fAdp_DarkenDynamic * darken_dynamic;
        darken = lerp(darken, -min(1-pixellum, -darken), fAdp_WhiteProtection);
        color.xyz += clamp(darken, -fAdp_MaxDarken, 0);
    }

    #if (iAdp_DebugOutput == 1)
        color = avglum;
    #endif

    return color;

}


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Techniques                                                                                                                //
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

technique Adaption
{
    pass Luminance
    {
        VertexShader = VS_Main;
        PixelShader = PS_Luminance;
        RenderTarget = texLuminance;
    }

    pass AvgLuminance
    {
        VertexShader = VS_Main;
        PixelShader = PS_AvgLuminance;
        RenderTarget = texAvgLuminance;
    }

    pass Adaption
    {
        VertexShader = VS_Main;
        PixelShader = PS_Adaption;
    }

    pass StoreAvgLuminance
    {
        VertexShader = VS_Main;
        PixelShader = PS_StoreAvgLuminance;
        RenderTarget = texAvgLuminanceLast;
    }
}
