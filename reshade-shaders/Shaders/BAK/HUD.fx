texture BackBufferTex : COLOR;
texture HudColorTex < source = "lutAtlasT.png"; > { Width = 2560; Height = 1080; Format = RGBA8; };
sampler BackBuffer { Texture = BackBufferTex; };
sampler HudColor { Texture = HudColorTex; };

uniform float3 TransparencyControl <
	ui_type = "drag";
	ui_min = 0.0; ui_max = 1.0;
> = float3(1.0, 1.0, 1.0);

void PostProcessVS(uint id : SV_VertexID, out float4 position : SV_Position, out float2 texcoord : TEXCOORD)
{
	texcoord.x = (id == 2) ? 2.0 : 0.0;
	texcoord.y = (id == 1) ? 2.0 : 0.0;
	position = float4(texcoord * float2(2.0, -2.0) + float2(-1.0, 1.0), 0.0, 1.0);
}
float3 HudPS(float4 vpos : SV_Position, float2 texcoord : TEXCOORD) : SV_Target
{
	float4 hud = tex2D(HudColor, texcoord);
	float3 color = tex2D(BackBuffer, texcoord).rgb;

	return lerp(color, hud.rgb, hud.aaa * TransparencyControl);
}

technique HUD
{
	pass
	{
		VertexShader = PostProcessVS;
		PixelShader = HudPS;
	}
}