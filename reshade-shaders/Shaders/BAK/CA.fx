//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ReShade effect file
// visit facebook.com/MartyMcModding for news/updates
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Advanced Depth of Field lite by Marty McFly
// For private use only!
// Copyright © 2008-2015 Marty McFly
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define iADOF_ImageChromaHues           10                      //[2 to 20] Amount of samples through the light spectrum to get a smooth gradient.
#define fADOF_ImageChromaCurve          0.5                     //[0.5 to 2.0] Image chromatic aberration curve. Higher means less chroma at screen center areas.
#define fADOF_ImageChromaAmount         15                  //[5.0 to 200.0] Linearly increases image chromatic aberration amount.


/////////////////////////TEXTURES / INTERNAL PARAMETERS/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////TEXTURES / INTERNAL PARAMETERS/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#define ScreenSize      float4(BUFFER_WIDTH, BUFFER_RCP_WIDTH, float(BUFFER_WIDTH) / float(BUFFER_HEIGHT), float(BUFFER_HEIGHT) / float(BUFFER_WIDTH)) //x=Width, y=1/Width, z=ScreenScaleY, w=1/ScreenScaleY
#define PixelSize       float2(BUFFER_RCP_WIDTH, BUFFER_RCP_HEIGHT)
uniform float  Timer < source = "timer"; >;

texture2D texColor : COLOR;

sampler2D SamplerColor
{
        Texture = texColor;
        MinFilter = LINEAR;
        MagFilter = LINEAR;
        MipFilter = LINEAR;
        AddressU = Clamp;
        AddressV = Clamp;
};

/////////////////////////VERTEX SHADER//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////VERTEX SHADER//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct VS_OUTPUT_POST
{
        float4 vpos  : POSITION;
        float2 txcoord : TEXCOORD0;
};

VS_OUTPUT_POST VS_PostProcess(in uint id : SV_VertexID)
{
        VS_OUTPUT_POST OUT;
        OUT.txcoord.x = (id == 2) ? 2.0 : 0.0;
        OUT.txcoord.y = (id == 1) ? 2.0 : 0.0;
        OUT.vpos = float4(OUT.txcoord * float2(2.0, -2.0) + float2(-1.0, 1.0), 0.0, 1.0);
        return OUT;
}

/////////////////////////PIXEL SHADERS//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////PIXEL SHADERS//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


float4 PS_Chroma(VS_OUTPUT_POST IN) : COLOR
{
        float4 scenecolor = 0;

        float2 coord=IN.txcoord.xy*2.0-1.0;
        float offsetfact=length(IN.txcoord.xy*2.0-1.0);
        offsetfact=pow(offsetfact,fADOF_ImageChromaCurve)*fADOF_ImageChromaAmount*BUFFER_RCP_WIDTH;

        float3 chromaweight = 0.0;

        [unroll]
        for (float c=0; c<iADOF_ImageChromaHues && c < 90; c++)
        {
                float temphue = c/iADOF_ImageChromaHues;
                float3 tempchroma = saturate(float3(abs(temphue * 6.0 - 3.0) - 1.0,2.0 - abs(temphue * 6.0 - 2.0),2.0 - abs(temphue * 6.0 - 4.0)));
                float  tempoffset = (c + 0.5)/iADOF_ImageChromaHues - 0.5;
                float3 tempsample = tex2Dlod(SamplerColor, float4(coord.xy*(1.0+offsetfact*tempoffset)*0.5+0.5,0,0)).xyz;
                scenecolor.xyz += tempsample.xyz*tempchroma.xyz;
                chromaweight += tempchroma;
        }
        //not all hues have the same brightness, FF0000 and FFFF00 are obviously differently bright but are just different hues.
        //there is no generic way to make it work for all different hue options. Sometimes / samples * 0.5 works, then * 0.666, then something completely different.
        scenecolor.xyz /= dot(chromaweight.xyz, 0.333);

        return scenecolor;
}

/////////////////////////TECHNIQUES/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////TECHNIQUES/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

technique CA
{
        pass Chroma
        {
                VertexShader = VS_PostProcess;
                PixelShader = PS_Chroma;
        }
}
